#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from csv import DictWriter
from urllib.parse import urlsplit

from googleapiclient.discovery import build
from dateutil.parser import parse

########################################################################################################################
# INPUTS

# Put in desired channels to get the most recent video for here
CHANNEL_URLS = [
    'https://www.youtube.com/channel/UCyNtlmLB73-7gtlBz00XOQQ',  # Folding Ideas
    'https://www.youtube.com/channel/hbomberguy'  # Hbomb
]

# Where to write the results, relative to the execution path
OUTPUT_FILE = 'test.csv'


########################################################################################################################

def get_channel_id_from_username(client, username):
    response = client.channels().list(part='id', forUsername=username).execute()
    return response['items'][0]['id']


def get_channel_info(client, channel_id, part='snippet,statistics'):
    response = client.channels().list(id=channel_id, part=part).execute()
    return response['items'][0]


def get_most_recent_video_id(client, channel_id, part='id'):
    response = client.search().list(type='video', channelId=channel_id, part=part, order='date', maxResults=1).execute()
    return response['items'][0]['id']['videoId']


def get_video_info(client, video_id, part='snippet,contentDetails,statistics'):
    response = client.videos().list(id=video_id, part=part).execute()
    return response['items'][0]


API_KEY = os.environ['API_KEY']
client = build('youtube', 'v3', developerKey=API_KEY)

channel_ids = []
for url in CHANNEL_URLS:
    channel_id = os.path.split(urlsplit(url).path)[1]
    if not channel_id.startswith('UC'):
        channel_id = get_channel_id_from_username(client, channel_id)
    channel_ids.append(channel_id)

channel_infos = [get_channel_info(client, channel_id) for channel_id in channel_ids]
video_ids = [get_most_recent_video_id(client, channel_id) for channel_id in channel_ids]
video_info = [get_video_info(client, video_id) for video_id in video_ids]

with open(OUTPUT_FILE, 'w') as f:
    writer = DictWriter(f, ['channel_title', 'channel_url', 'video_title', 'video_uploaded', 'video_url'])
    writer.writeheader()

    for channel_info, video_info in zip(channel_infos, video_info):
        writer.writerow({
            'channel_title': channel_info['snippet']['title'],
            'channel_url': f'https://youtube.com/channel/{channel_info["id"]}',
            'video_title': video_info['snippet']['title'],
            'video_uploaded': parse(video_info['snippet']['publishedAt']),
            'video_url': f'https://www.youtube.com/watch?v={video_info["id"]}'
        })
