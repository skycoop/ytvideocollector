# YTVideoCollector

This is a slim proof of concept for a script that gets the most recent video for a list of channels and outputs a CSV
fit for use with Google Sheets or Excel. This can easily be beefed up into something that outputs more metadata, or is
easier to run. Due to time constraints, this documentation will assume familiarity with the command line, Python, 
Pipenv, and the Google Application Developer Console. The script should run on Windows, Mac, or Linux, but only Linux
has been tested.

## Setup
This project manages dependencies with [Pipenv](https://pipenv.readthedocs.io/en/latest/). Install dependencies with
```
$ pipenv install
```

## Running
First modify the `CHANNEL_URL` list and `OUTPUT_FILE` as you see fit. You will also need a Google API key from a project
with the YouTube Data v3 API enabled.

The script can then be run by
```
$ API_KEY="[your API key]" pipenv run main
```

This will create an CSV file named `OUTPUT_FILE`. A sample CSV file is [here](assets/test.csv).

### Using with Google Sheets
Open a new sheet. Select "File" > "Import...". Select the "Upload" tab, and upload the created file. In the "Import 
file" window that opens, make sure "Convert text to numbers and dates" is set to "Yes".